import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.concurrent.TimeoutException;

import javax.print.DocFlavor.STRING;

import com.rabbitmq.client.*;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import org.apache.commons.io.*;

public class FileSystemServer {

    private static final String RPC_QUEUE_NAME = "RPC_QUEUE";
    private String BROADCAST_RPC_QUEUE_NAME;
    private static final String BROADCAST_EXCHANGE_NAME = "BROADCAST_EXCHANGE";
    private static final String RPC_QUEUE_CONSUMER_TAG = "RPC_REQUEST";
    private static final String BROADCAST_RPC_QUEUE_CONSUMER_TAG = "BROADCAST_RPC_REQUEST";

    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    private File storageFolder;

    public FileSystemServer(String hostname) throws IOException, TimeoutException
    {
        storageFolder = new File("storage").getAbsoluteFile();

        factory = new ConnectionFactory();
        factory.setHost(hostname);

        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.basicQos(1);    // Each server will only have one message assigned to it at a time

        channel.exchangeDeclare(BROADCAST_EXCHANGE_NAME, "fanout");

        // non-durable, exclusive, autoDelete
        BROADCAST_RPC_QUEUE_NAME = channel.queueDeclare().getQueue();

        // Parameters:
        // - queueName
        // - exchangeName
        // - routingKey
        channel.queueBind(BROADCAST_RPC_QUEUE_NAME, BROADCAST_EXCHANGE_NAME, "");

        // Parameters:
        // - queueName
        // - autoAcknowledge (ALERT: don't know if this should be set to true, don't know how acknowledgement in a fanout exchange works)
        // - Callback
        // - Consumer Tag (still don't really know what this does)
        channel.basicConsume(BROADCAST_RPC_QUEUE_NAME, true, BROADCAST_RPC_QUEUE_CONSUMER_TAG, defaultConsumer);
        // channel.basicConsume(BROADCAST_RPC_QUEUE_NAME, true, clientMessageCallback, (consumerTag -> { }));

        // Parameters:
        // - queueName
        // - durable (whether or not it surviver a server restart)
        // - exclusive (if this queue is restricted to this connection)
        // - autoDelete (if it deletes itself when it has no more consumers)
        channel.queueDeclare(RPC_QUEUE_NAME, false, false, true, null);

        // Parameters:
        // - queueName
        // - autoAcknowledge
        // - Callback
        // - Consumer Tag (still don't really know what this does)
        channel.basicConsume(RPC_QUEUE_NAME, false, RPC_QUEUE_CONSUMER_TAG, defaultConsumer);
        // channel.basicConsume(RPC_QUEUE_NAME, false, clientMessageCallback, consumerTag -> { });
    }

    private DefaultConsumer defaultConsumer = new DefaultConsumer(channel)
    {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
        {
            long deliveryTag = envelope.getDeliveryTag();

            if(!storageFolder.exists())
            {
                if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                    RejectRequest(deliveryTag, true, "Storage folder doesn't exist locally. Please restart the server");
                else
                    log("Storage folder doesn't exist locally. Please restart the server");

                return;
            }

            if(properties.getReplyTo() == null)
            {
                if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                    RejectRequest(deliveryTag, false, "The message doesn't specify a reply queue");
                else
                    log("The message doesn't specify a reply queue");
                
                return;
            }

            // Dissecting the received message
            String message = new String(body, "UTF-8");

            log("Received message: " + message);

            String[] messageParts = message.split("\\<\\*>");

            if(messageParts.length < 2)
            {
                if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                    RejectRequest(deliveryTag, false, "The message didn't have enough parameters");
                else
                    log("The message didn't have enough parameters");
                    
                return;
            }

            String request = messageParts[0];
            String path = messageParts[1];

            path = path.replaceAll("\\s+","");

            String result = null;

            if(request.equals("list"))
            {
                log("List request received to path " + path);

                String[] directories = List(path);

                if(directories != null)
                {
                    result = "";

                    for (String dir : directories) {
                        result += dir + "<*>";
                    }
                }
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "";
            }
            else if(request.equals("exists"))
            {
                log("Exists request received to path " + path);

                if(ExistsDir(path))
                    result = "true";
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "false";
            }
            else if(request.equals("size"))
            {
                log("Size request received to path " + path);

                long fileSize = Size(path);

                if(fileSize != -1)
                    result = String.valueOf(fileSize);
            }
            else if(request.equals("read"))
            {
                log("Read request received to path " + path);

                String data = Read(path);

                File file = new File(storageFolder, path);

                String filename = file.getName();

                if(data != null)
                {
                    result = filename + "<*>" + data;   // Sends filename and data
                }   
            }
            else if(request.equals("write"))
            {
                log("Write request received to path " + path);

                if(messageParts.length < 3)
                {
                    if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                        RejectRequest(deliveryTag, false, "The message didn't have enough parameters");
                    else
                        log("The message didn't have enough parameters");

                    return;
                }

                String data = messageParts[2];

                boolean success = Write(path, data);

                if(success)
                    result = "true";
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "false";
            }
            else if(request.equals("createDir"))
            {
                log("CreateDir request received to path " + path);

                boolean success = CreateDir(path);

                if(success)
                    result = "true";
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "false";
            }
            else if(request.equals("createFile"))
            {
                log("CreateFile request received to path " + path);

                boolean success;

                if(messageParts.length < 3)
                {
                    success = CreateFile(path);
                }
                else
                {
                    String data = messageParts[2];
                    
                    success = CreateFile(path, data);
                }

                if(success)
                    result = "true";
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "false";
            }
            else if(request.equals("delete"))
            {
                log("Delete request received to path " + path);

                if(Delete(path))
                    result = "true";
                else if(consumerTag.equals(BROADCAST_RPC_QUEUE_CONSUMER_TAG))
                    result = "false";
            }

            if(result == null)
            {
                if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                    RejectRequest(deliveryTag, true, "Couldn't get result.");
                else
                    log("Couldn't get result.");

                return;
            }

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                .Builder()
                .correlationId(properties.getCorrelationId())
                .build();

            channel.basicPublish("", properties.getReplyTo(), replyProps, result.getBytes("UTF-8"));

            if(consumerTag.equals(RPC_QUEUE_CONSUMER_TAG))
                AckRequest(deliveryTag, "Request completed");
            else
                log("Request completed");
        }
    };

    private boolean CreateDir(String path)
    {
        File directory = new File(storageFolder, path);

        if(directory.exists())
            return false;

        directory.getParentFile().mkdirs();

        if(!directory.mkdir())
            return false;

        return true;
    }

    private boolean CreateFile(String path)
    {
        return CreateFile(path, null);
    }   

    // Creates a new file, with the given data (optional), returns if it was successful or not
    private boolean CreateFile(String path, String data)
    {
        File directory = new File(storageFolder, path);

        if(directory.exists())
            return false;

        directory.getParentFile().mkdirs();

        boolean created;
            
        try
        {
            created = directory.createNewFile();
        }
        catch (IOException ex)
        {
            return false;
        }

        if(!created)
            return false;

        if(data != null)
        {
            if(!Write(path, data))
                return false; 
        }

        return true;
    }

    // Reads a file and sends its bytes (in Base64) in a string
    private String Read(String path)
    {
        File directory = new File(storageFolder, path);

        if(!directory.exists())
            return null;  

        if(!directory.isFile())
            return null;   

        byte[] data;

        try
        {
            data = FileUtils.readFileToByteArray(directory);
        }
        catch(IOException ex)
        {
            log("Failed to convert file to byte array.");
            return null;
        }

        String encoded = Base64.getEncoder().encodeToString(data);
        
        return encoded;
    }

    // Writes on an existing file, returns if it was successful or not
    private boolean Write(String path, String data)
    {
        File directory = new File(storageFolder, path);

        if(!directory.exists())
            return false;

        if(!directory.isFile())
            return false;

        if(data == null)
            return false;

        byte[] byteArray = Base64.getDecoder().decode(data);

        try
        {
            FileUtils.writeByteArrayToFile(directory, byteArray);
        } catch(IOException ex)
        {
            return false;
        }

        return true;
    }

    // Returns the size of a file in bytes
    private long Size(String path)
    {
        File directory = new File(storageFolder, path);

        if(!directory.exists())
            return -1;

        if(!directory.isFile())
            return -1;

        long size = directory.length();

        return size;
    }

    // Returns the list of files and directories inside a given path
    private String[] List(String path)
    {
        File directory = new File(storageFolder, path);        

        if(!directory.exists())
        {
            log("Specified path doesn't exist.");
            return null;
        }

        if(!directory.isDirectory())
        {
            log("Specified path isn't a directory.");
            return null;
        }

        return directory.list();
    }

    // Deletes a file, returns if it was successful or not
    private boolean Delete(String path)
    {
        File directory = new File(storageFolder, path);

        if(!directory.exists())
            return false;

        if(directory.isDirectory())
        {
            if(directory.list().length > 0)
            {
                for (String dir : directory.list())
                {
                    if(!Delete(path + "\\" + dir))
                        return false;
                }
            }
        }

        if(!directory.delete())
            return false;

        return true;
    }

    // Checks if a given path directory exists
    private boolean ExistsDir(String path)
    {
        File directory = new File(storageFolder, path);

        if(!directory.exists())
            return false;

        if(!directory.isDirectory())
            return false;

        return true;
    }

    public static void main(String[] args){
        if(args.length != 1)
        {
            log("No hostname was given. Shutting down application");
            System.exit(1);
        }

        File storageFolder = new File("storage").getAbsoluteFile();

        log("Server location: " + storageFolder.getParentFile().getAbsolutePath());

        if(!storageFolder.exists())
        {
            if(!storageFolder.mkdir())
            {
                log("Error creating storage folder. Shutting down application");
                System.exit(1);
            }
            else
                log("Storage folder created");
        }

        try
        {
            FileSystemServer server = new FileSystemServer(args[0]);
        }
        catch(IOException | TimeoutException e)
        {
            log("Exception was thrown: " + e.getMessage() + ". Shutting down application.");
            System.exit(1);
        }

        log("Server started");
    }

    private static void log(String msg)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // So the server doesn't block while logging
        if(msg.length() >= 1000)
            msg = msg.substring(0, 999) + "...";

        System.out.println(LocalDateTime.now().format(formatter) + " - " + msg);
    }

    private void AckRequest(long deliveryTag) throws IOException
    {
        log("Request acknowledged");
        channel.basicAck(deliveryTag, false);
    }

    private void AckRequest(long deliveryTag, String message) throws IOException
    {
        log("Request acknowledged: " + message);
        channel.basicAck(deliveryTag, false);
    }

    private void RejectRequest(long deliveryTag, boolean requeue) throws IOException
    {
        log("Request rejected");
        channel.basicReject(deliveryTag, requeue);
    }

    private void RejectRequest(long deliveryTag, boolean requeue, String message) throws IOException
    {
        log("Request rejected: " + message);
        channel.basicReject(deliveryTag, requeue);
    }
}