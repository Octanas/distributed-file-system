﻿using ClientLogicLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIClient
{
    public partial class Form1 : Form
    {
        private delegate int SafeCallDelegate(string path,int timeout);
        CommunicationManager ComMan;
        //List<string> history;
        bool res;
        string[] nodeDirs;

        public Form1()
        {
            InitializeComponent();

            ComMan = new CommunicationManager("localhost");
            //history = new List<string>();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread t = new Thread(() =>
            {
                Loading("begin", "Loading directories from server.");

                PopulateTreeViewNew();

                this.treeView1.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClickNew);

                Loading("end", "Initial load completed.", true);

                HandleButtons();

            });
            t.IsBackground = true;
            t.Start();
        }

        public void HandleButtons()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(HandleButtons));
                return;
            }
            else
            {
                buttonCreateDir.Enabled = true;
                buttonCreateFile.Enabled = true;
                buttonDelete.Enabled = true;
                buttonRead.Enabled = true;
                buttonUpdateFile.Enabled = true;
            }
        }

        private void PopulateTreeViewNew()
        {
            string localroot = @" ";
            TreeNode rootNode = new TreeNode(localroot);

            string[] subdirs;
            if (ComMan.List(localroot, 10, out subdirs) == -1)
                return;

            rootNode = GetDirectoriesNew(subdirs, rootNode, localroot);

            if(InvokeRequired)
            {
                Invoke((MethodInvoker)(() => treeView1.Nodes.Add(rootNode)));
            }
            else
                treeView1.Nodes.Add(rootNode);
        }

        private TreeNode GetDirectoriesNew(string[] subdirs, TreeNode nodeToAddTo, string path)
        {
            TreeNode anode;
            string[] subSubdirs;
            foreach (string subdir in subdirs)
            {
                string newpath = path + @"\" + subdir;

                if (ComMan.ExistsDir(newpath, 10, out bool result) == 0)
                {
                    if (result)
                    {
                        anode = new TreeNode(subdir, 0, 0);
                        anode.ImageIndex = 0;

                        if (ComMan.List(newpath, 10, out subSubdirs) == 0)
                        {
                            if (subSubdirs.Length != 0)
                            {
                                GetDirectoriesNew(subSubdirs, anode, newpath);
                            }
                        }
                        nodeToAddTo.Nodes.Add(anode);
                    }
                }
                else
                    return null;
            }
            return nodeToAddTo;
        }

        private void treeView1_NodeMouseClickNew(object sender, TreeNodeMouseClickEventArgs e)
        {
            comboBoxNav.Text = e.Node.FullPath;

            listView1.Items.Clear();

            nodeDirs = null;
            Thread t = new Thread(() =>
            {
                Loading("begin", "Loading directories from server.");

                if (ComMan.List(e.Node.FullPath, 10, out nodeDirs) == 0)
                {
                    Loading("end", "Directories loaded successfully,", true);

                    foreach (string dir in nodeDirs)
                    {
                        DirListHandler(dir, e.Node.FullPath);
                    }
                }
                else
                {
                    Loading("end", "Could not load directories from server.", false);
                }
            });
            t.IsBackground = true;
            t.Start();
        }

        private void DirListHandler(string dir,string path)
        {
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;

            if (InvokeRequired)
            {
                BeginInvoke(new Action<string,string>(DirListHandler), dir,path);
                return;
            }
            else
            {
                if(ComMan.ExistsDir(path + @"\" + dir, 10, out bool result) == 0)
                {
                    if (result)
                    {
                        item = new ListViewItem(dir, 0);
                        subItems = new ListViewItem.ListViewSubItem[] { new ListViewItem.ListViewSubItem(item, "Directory") };
                        item.SubItems.AddRange(subItems);
                        listView1.Items.Add(item);
                    }
                    else
                    {
                        Loading("begin", "Loading directories from server.");

                        long size = 0;

                        if (ComMan.Size(path + @"\" + dir, 10, out size) == 0)
                            Loading("end", "Directories loaded successfully,", true);
                        else
                            Loading("end", "Could not load directories from server.", false);

                        item = new ListViewItem(dir, 1);
                        subItems = new ListViewItem.ListViewSubItem[] { new ListViewItem.ListViewSubItem(item, "File"), new ListViewItem.ListViewSubItem(item, size.ToString()) };
                        item.SubItems.AddRange(subItems);
                        listView1.Items.Add(item);

                    }
                }
            }
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            if(listView1.FocusedItem== null || listView1.FocusedItem.SubItems[1].Text == "Directory")
                MessageBox.Show("Please select the file from the List View on the right that you wish to overwrite.", "Please select an item", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                string path = comboBoxNav.Text + @"\" + listView1.FocusedItem.Text;

                Thread t = new Thread(() =>
                {
                    Loading("begin", "Downloading file from server.");

                    if (ComMan.Read(path, 10) == 0)
                    {
                        Loading("end", "File downloaded successfully,", true);
                    }
                    else
                    {
                        Loading("end", "Could not open file.", false);
                    }
                });
                t.IsBackground = true;
                t.Start();
            }
        }

        private void buttonCreateFile_click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            DialogResult result = open.ShowDialog();

            if (result == DialogResult.OK)
            {
                string localpath = open.FileName;

                string remotepath = comboBoxNav.Text + @"\" + open.SafeFileName;

                Thread t = new Thread(() =>
                {
                    Loading("begin", "Uploading file to server.");

                    if (ComMan.CreateFile(remotepath, localpath, 10, out res)==0)
                    {
                        if (res)
                        {
                            //success
                            Loading("end", "File uploaded successfully,", true);
                            ClearListHandler();
                        }
                        else
                        {
                            //fail
                            Loading("end", "Could not upload file to server.", false);
                        }
                    }
                    else
                    {
                        Loading("end", "Could not access server.", false);
                    }
                });
                t.IsBackground = true;
                t.Start();
            }
        }

        private void buttonUpdateFile_Click(object sender, EventArgs e)
        {
            if (listView1.FocusedItem == null || listView1.FocusedItem.SubItems[1].Text == "Directory")
            {
                MessageBox.Show("Please select the file from the List View on the right that you wish to overwrite.", "Please select an item", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dgr = MessageBox.Show("Are you sure you wish to overwrite the selected item? This action cannot be reversed.", "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (dgr==DialogResult.OK)
                {
                    OpenFileDialog open = new OpenFileDialog();

                    DialogResult result = open.ShowDialog();

                    if (result == DialogResult.OK)
                    {
                        string localpath = open.FileName;

                        string remotepath = comboBoxNav.Text + @"\" + listView1.FocusedItem.Text;

                        Thread t = new Thread(() =>
                        {
                            Loading("begin", "Uploading file to server.");

                            if (ComMan.Write(remotepath, localpath, 10, out res) == 0)
                            {
                                if (res)
                                {
                                    //success
                                    Loading("end", "File uploaded successfully,", true);
                                    ClearListHandler();
                                }
                                else
                                {
                                    //fail
                                    Loading("end", "Could not upload file to server.", false);
                                }
                            }
                            else
                            {
                                Loading("end", "Could not access server.", false);
                            }
                        });
                        t.IsBackground = true;
                        t.Start();
                    }
                }
            }
        }

        private void buttonCreateDir_Click(object sender, EventArgs e)
        {
            CreateDir inputwindow = new CreateDir();
            if (inputwindow.ShowDialog()==DialogResult.OK)
            {
                string path = comboBoxNav.Text + @"\" + inputwindow.input;

                Thread t = new Thread(() =>
                {
                    Loading("begin", "Creating directory.");

                    if (ComMan.CreateDir(path, 10, out res) == 0)
                    {
                        if (res)
                        {
                            //success
                            Loading("end", "Directory created successfully.", true);
                            HandlePopulate();
                            ClearListHandler();
                        }
                        else
                        {
                            //fail
                            Loading("end", "Could not create directory.", false);
                        }
                    }
                    else
                    {
                        Loading("end", "Could not access server.", false);
                    }
                });
                t.IsBackground = true;
                t.Start();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.FocusedItem!=null)
            {
                DialogResult result;

                result = MessageBox.Show("Are you sure you want to permanently delete these files? This action cannot be reversed", "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.OK)
                {
                    string path = comboBoxNav.Text + "\\" + listView1.FocusedItem.Text;

                    Thread t = new Thread(() =>
                    {
                        Loading("begin", "Deleting files.");

                        if (ComMan.Delete(path, 10, out res) == 0)
                        {
                            if (res)
                            {
                                //success
                                Loading("end", "Files deleted successfully.", true);
                                HandlePopulate();
                                ClearListHandler();
                            }
                            else
                            {
                                //fail
                                Loading("end", "Could not delete files.", false);
                            }
                        }
                        else
                        {
                            Loading("end", "Could not access server.", false);
                        }
                    });
                    t.IsBackground = true;
                    t.Start();
                }
            }
            else
            {
                MessageBox.Show("Please select an item from the List View on the right.","Item not selected",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void ClearListHandler()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(ClearListHandler));
            }
            else
            {
                listView1.Items.Clear();
            }
        }

        private void HandlePopulate()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(HandlePopulate));
                return;
            }
            else
            {
                treeView1.Nodes.Clear();
                PopulateTreeViewNew();
                treeView1.Update();
            }
        }

        private void Loading(string action, string actionText, bool successful=false)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<string,string,bool>(Loading), action,actionText,successful);
                return;
            }
            pictureFail.Visible = false;
            pictureSuccess.Visible = false;
            pictureLoading.Visible = false;
            textBoxStatus.Text = actionText;

            if (action=="begin")
                pictureLoading.Visible = true;
            else if (action=="end" && successful)
                pictureSuccess.Visible = true;
            else if (action == "end" && !successful)
                pictureFail.Visible = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);
        }
    }
}
