﻿namespace GUIClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonCreateFile = new System.Windows.Forms.Button();
            this.buttonCreateDir = new System.Windows.Forms.Button();
            this.comboBoxNav = new System.Windows.Forms.ComboBox();
            this.labelNavigate = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonDelete = new System.Windows.Forms.Button();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.pictureSuccess = new System.Windows.Forms.PictureBox();
            this.pictureFail = new System.Windows.Forms.PictureBox();
            this.pictureLoading = new System.Windows.Forms.PictureBox();
            this.buttonUpdateFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSuccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // buttonRead
            // 
            this.buttonRead.Enabled = false;
            this.buttonRead.Location = new System.Drawing.Point(19, 233);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(116, 43);
            this.buttonRead.TabIndex = 1;
            this.buttonRead.Text = "Open File";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonCreateFile
            // 
            this.buttonCreateFile.Enabled = false;
            this.buttonCreateFile.Location = new System.Drawing.Point(19, 163);
            this.buttonCreateFile.Name = "buttonCreateFile";
            this.buttonCreateFile.Size = new System.Drawing.Size(116, 43);
            this.buttonCreateFile.TabIndex = 2;
            this.buttonCreateFile.Text = "Upload File";
            this.buttonCreateFile.UseVisualStyleBackColor = true;
            this.buttonCreateFile.Click += new System.EventHandler(this.buttonCreateFile_click);
            // 
            // buttonCreateDir
            // 
            this.buttonCreateDir.Enabled = false;
            this.buttonCreateDir.Location = new System.Drawing.Point(19, 93);
            this.buttonCreateDir.Name = "buttonCreateDir";
            this.buttonCreateDir.Size = new System.Drawing.Size(116, 43);
            this.buttonCreateDir.TabIndex = 3;
            this.buttonCreateDir.Text = "Create Folder";
            this.buttonCreateDir.UseVisualStyleBackColor = true;
            this.buttonCreateDir.Click += new System.EventHandler(this.buttonCreateDir_Click);
            // 
            // comboBoxNav
            // 
            this.comboBoxNav.FormattingEnabled = true;
            this.comboBoxNav.Location = new System.Drawing.Point(152, 36);
            this.comboBoxNav.Name = "comboBoxNav";
            this.comboBoxNav.Size = new System.Drawing.Size(300, 21);
            this.comboBoxNav.TabIndex = 1;
            // 
            // labelNavigate
            // 
            this.labelNavigate.AutoSize = true;
            this.labelNavigate.Location = new System.Drawing.Point(93, 39);
            this.labelNavigate.Name = "labelNavigate";
            this.labelNavigate.Size = new System.Drawing.Size(53, 13);
            this.labelNavigate.TabIndex = 6;
            this.labelNavigate.Text = "Navigate:";
            // 
            // treeView1
            // 
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(152, 69);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(300, 311);
            this.treeView1.TabIndex = 9;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FolderImage.png");
            this.imageList1.Images.SetKeyName(1, "DocumentImage.png");
            this.imageList1.Images.SetKeyName(2, "OpenFolderImage.png");
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(471, 69);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(333, 311);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 127;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Type";
            this.columnHeader2.Width = 85;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "File Size";
            this.columnHeader3.Width = 82;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Location = new System.Drawing.Point(19, 364);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(116, 43);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStatus.Location = new System.Drawing.Point(588, 428);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(288, 18);
            this.textBoxStatus.TabIndex = 12;
            // 
            // pictureSuccess
            // 
            this.pictureSuccess.Image = global::GUIClient.Properties.Resources.success_icon;
            this.pictureSuccess.Location = new System.Drawing.Point(559, 426);
            this.pictureSuccess.Name = "pictureSuccess";
            this.pictureSuccess.Size = new System.Drawing.Size(24, 23);
            this.pictureSuccess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSuccess.TabIndex = 15;
            this.pictureSuccess.TabStop = false;
            this.pictureSuccess.Visible = false;
            // 
            // pictureFail
            // 
            this.pictureFail.Image = global::GUIClient.Properties.Resources.fail;
            this.pictureFail.Location = new System.Drawing.Point(559, 426);
            this.pictureFail.Name = "pictureFail";
            this.pictureFail.Size = new System.Drawing.Size(24, 23);
            this.pictureFail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureFail.TabIndex = 14;
            this.pictureFail.TabStop = false;
            this.pictureFail.Visible = false;
            // 
            // pictureLoading
            // 
            this.pictureLoading.Image = global::GUIClient.Properties.Resources.loading;
            this.pictureLoading.Location = new System.Drawing.Point(559, 426);
            this.pictureLoading.Name = "pictureLoading";
            this.pictureLoading.Size = new System.Drawing.Size(24, 23);
            this.pictureLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureLoading.TabIndex = 13;
            this.pictureLoading.TabStop = false;
            this.pictureLoading.Visible = false;
            // 
            // buttonUpdateFile
            // 
            this.buttonUpdateFile.Enabled = false;
            this.buttonUpdateFile.Location = new System.Drawing.Point(19, 300);
            this.buttonUpdateFile.Name = "buttonUpdateFile";
            this.buttonUpdateFile.Size = new System.Drawing.Size(116, 43);
            this.buttonUpdateFile.TabIndex = 16;
            this.buttonUpdateFile.Text = "Update File";
            this.buttonUpdateFile.UseVisualStyleBackColor = true;
            this.buttonUpdateFile.Click += new System.EventHandler(this.buttonUpdateFile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 450);
            this.Controls.Add(this.buttonUpdateFile);
            this.Controls.Add(this.pictureSuccess);
            this.Controls.Add(this.pictureFail);
            this.Controls.Add(this.pictureLoading);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.labelNavigate);
            this.Controls.Add(this.comboBoxNav);
            this.Controls.Add(this.buttonCreateDir);
            this.Controls.Add(this.buttonCreateFile);
            this.Controls.Add(this.buttonRead);
            this.Name = "Form1";
            this.Text = "LimeWire Napster 5200.V2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureSuccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonCreateFile;
        private System.Windows.Forms.Button buttonCreateDir;
        private System.Windows.Forms.ComboBox comboBoxNav;
        private System.Windows.Forms.Label labelNavigate;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.PictureBox pictureLoading;
        private System.Windows.Forms.PictureBox pictureFail;
        private System.Windows.Forms.PictureBox pictureSuccess;
        private System.Windows.Forms.Button buttonUpdateFile;
    }
}

