﻿using System;
using System.Drawing;
using System.Threading;
using ClientLogicLibrary;

namespace CommandLineClient
{
    class Program
    {
        private static bool loadingIndicator = false;
        private static int loadingCounter = 0;
        private static Timer loadingTimer = new Timer(obj =>
        {
            if(loadingIndicator)
            {
                if (loadingCounter == 0)
                {
                    ClearCurrentConsoleLine();
                    Console.Write("Loading");
                }
                else if (loadingCounter == 1)
                    Console.Write(".");
                else if (loadingCounter == 2)
                    Console.Write(".");
                else if (loadingCounter == 3)
                    Console.Write(".");

                if (loadingCounter == 3)
                    loadingCounter = 0;
                else
                    loadingCounter++;
            }
        });

        public static String CurrentPath { get; private set; }
        public static String Input { get; private set; }

        static void Main(string[] args)
        {
            CurrentPath = " ";
            CommunicationManager comMan = new CommunicationManager("localhost");

            loadingTimer.Change(0, 800);    // Starts the loadingTimer (it will always be running)

            do
            {
                if (CurrentPath.StartsWith(" \\"))
                    Console.Write(CurrentPath.Substring(2));

                Console.Write("> ");
                Input = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(Input))
                {
                    String[] InputList = Input.Split(" ");
                    string output = "";

                    Thread t = new Thread(() =>
                    {
                        switch (InputList[0].ToLower())
                        {
                            case "help":
                                if (InputList.Length==1)
                                {
                                    output = "pwd - Gets the current path;\n" +
                                             "ls/dir - Shows folders and paths in the current path;\n" +
                                             "size - Returns the size of a choosen file;\n" +
                                             "cd - Navigate into a directory;\n" +
                                             "mkdir - Create a directory;\n" +
                                             "mk - create a file;\n" +
                                             "save - create a copy of a local file on the remote server;\n" +
                                             "open - download and open a file from the server;\n" +
                                             "rm/del - Delete a file/directory;\n" +
                                             "cls - clear the screen";
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: help";
                                }
                                break;

                            //Command to get current path
                            case "pwd":
                                if (InputList.Length == 1)
                                {
                                    output = CurrentPath;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: pwd";
                                }
                                break;

                            //Commands to show directories
                            case "ls":
                            case "dir":
                                if (InputList.Length == 1)
                                {
                                    if (comMan.List(CurrentPath, 10, out string[] list) == 0)
                                    {
                                        foreach (string paths in list)
                                        {
                                            output += paths + "\n";
                                        }

                                        if (output.Length > 0)
                                            output = output.Remove(output.Length - 1);
                                    }
                                    else
                                    {
                                        output = "Error!";
                                    }
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: dir  or  ls";
                                }
                                break;

                            //Command to show a file's size
                            case "size":
                                if (InputList.Length == 2)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.Size(pathstring, 10, out long res) == 0)
                                        {
                                            output = res + " bytes";
                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: size [FileName]";
                                }
                                break;       

                            //Command navigate into a directory
                            case "cd": 
                                if (InputList.Length == 2)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.ExistsDir(pathstring, 10, out bool res) == 0)
                                        {
                                            if (res)
                                            {
                                                CurrentPath = pathstring;
                                            }
                                            else
                                            {
                                                output = "Directory does not exist.";
                                            }
                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: cd [DirectoryName]";
                                }
                                break;

                            //Command to create directory
                            case "mkdir": 
                                if (InputList.Length == 2)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.CreateDir(pathstring, 10, out bool res) == 0)
                                        {
                                            if (res)
                                            {
                                                output = "Directory successfully created.";
                                            }
                                            else
                                            {
                                                output = "Unable to create Directory.";
                                            }
                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: mkdir [DirectoryDestination]";
                                }
                                break;

                            //Command to create a file
                            case "mk": 
                                if (InputList.Length > 1 && InputList.Length < 4)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (InputList.Length == 2)
                                        {
                                            //Creates an empty file
                                            if (comMan.CreateFile(pathstring, 10, out bool res1) == 0)
                                            {
                                                if (res1)
                                                {
                                                    output = "Empty File successfully created.";
                                                }
                                                else
                                                {
                                                    output = "Unable to create Empty File.";
                                                }
                                            }
                                            else
                                            {
                                                output = "Error!";
                                            }
                                        }

                                        if (InputList.Length == 3)
                                        {
                                            //Creates a copy of a local file on the remote server
                                            if (comMan.CreateFile(pathstring, InputList[2], 10, out bool res1) == 0)
                                            {
                                                if (res1)
                                                {
                                                    output = "File successfully created.";
                                                }
                                                else
                                                {
                                                    output = "Unable to create Empty File.";
                                                }
                                            }
                                            else
                                            {
                                                output = "Error!";
                                            }
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: mk [DirectoryDestination]  or  mk [DirectoryDestination] [DirectorySource]";
                                }
                                break;

                            //Command that makes a copy of a local file on the remote server
                            case "save": 
                                if (InputList.Length == 3)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.Write(pathstring, InputList[2], 10, out bool res2) == 0)
                                        {
                                            if (res2)
                                            {
                                                output = "File successfully saved.";
                                            }
                                            else
                                            {
                                                output = "Unable to save File.";
                                            }
                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: save [FileDestination] [FileSource]";
                                }
                                break;

                            //Command that downloads file from server and opens it with the local deafult app for that type of file 
                            case "open": 
                                if (InputList.Length == 2)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.Read(pathstring, 10) == 0)
                                        {
                                            output = "File opened successfully.";
                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: open [FileLocation]";
                                }
                                break;

                            //Commands to delete file or directory
                            case "rm":
                            case "del": 
                                if (InputList.Length == 2)
                                {
                                    if (CleanPath(InputList[1], out string pathstring))
                                    {
                                        if (comMan.Delete(pathstring, 10, out bool res3) == 0)
                                        {
                                            if (res3)
                                            {
                                                output = "File successfully erased";

                                            }
                                            else
                                            {
                                                output = "File does not exist.";
                                            }

                                        }
                                        else
                                        {
                                            output = "Error!";
                                        }
                                    }
                                    else
                                        output = pathstring;
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: del [TargetLocation]  or  rm [TargetLocation]";
                                }
                                break;

                            //Command to clear the screen
                            case "cls":
                                if (InputList.Length==1)
                                {
                                    Console.Clear();
                                }
                                else
                                {
                                    output = "Wrong Parameters. Usage: cls";
                                }
                                break;

                            //Command not Recognized message
                            default:
                                output = "'" + Input + "' is not recognized as an internal or external command, operable program or batch file.";
                            break;
                        }

                        //Hide loading indicator
                        ToggleLoadingIndicator(false);

                    });

                    //Show loading indicator
                    ToggleLoadingIndicator(true);

                    t.Start();

                    while (loadingIndicator);

                    //Writes output
                    if (!string.IsNullOrWhiteSpace(output))
                        Console.WriteLine(output + "\n");
                    else
                        Console.WriteLine();
                }

            } while (true) ;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputPath"></param>
        /// <param name="output">Returns the error message in case of error, or the cleaned path in case it's successful.</param>
        /// <returns></returns>
        private static bool CleanPath(string inputPath, out string output)
        {
            if (inputPath.Contains("/") || inputPath.Contains("...") || inputPath.Contains("\\\\"))
            {
                output = "Invalid characters.";
                return false;
            }

            string pathstring;

            if (inputPath.StartsWith('\\'))
            {
                pathstring = " " + inputPath;
            }
            else
            {
                pathstring = CurrentPath + "\\" + inputPath;
            }

            if (pathstring.StartsWith(" \\.."))
            {
                output = "Can't go further back than the root.";
                return false;
            }

            string[] folders;
            folders = pathstring.Split('\\');

            string finalpath = "";

            for (int i = folders.Length - 1; i >= 0; i--)
            {
                if (folders[i] == "..")
                {
                    i--;
                }
                else if (folders[i] != ".")
                {
                    finalpath = folders[i] + "\\" + finalpath;
                }
            }

            output = finalpath.TrimEnd('\\');

            return true;
        }

        private static void ToggleLoadingIndicator(bool state)
        {
            if(state)
            {
                loadingIndicator = true;
            }
            else
            {
                ClearCurrentConsoleLine();
                loadingCounter = 0;
                loadingIndicator = false;
            }
        }

        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
