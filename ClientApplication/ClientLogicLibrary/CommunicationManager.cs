﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing.Impl;
using System.Runtime.Serialization;
using System.IO;
using System.Diagnostics;
using System.Linq;

namespace ClientLogicLibrary
{

    public class CommunicationManager
    {
        private Dictionary<String, String> Answer { get; set; }
        private Dictionary<String, int> AnswerCount { get; set; }

        private const String RPC_QUEUE_NAME = "RPC_QUEUE";
        private const String EXCHANGE_NAME = "BROADCAST_EXCHANGE";
        private ConnectionFactory factory;
        private IConnection connection;
        private IModel channel;
        private readonly string replyQueueName;
        private readonly EventingBasicConsumer consumer;
        private IBasicProperties props;

        public CommunicationManager(string hostName)
        {
            Answer = new Dictionary<string, string>();
            AnswerCount = new Dictionary<string, int>();

            factory = new ConnectionFactory() { HostName = hostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.ExchangeDeclare(exchange: EXCHANGE_NAME, type: "fanout");

            replyQueueName = channel.QueueDeclare().QueueName;
            consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var response = Encoding.UTF8.GetString(body);

                if (AnswerCount.ContainsKey(ea.BasicProperties.CorrelationId))
                {
                    if (AnswerCount[ea.BasicProperties.CorrelationId] > 0)
                        Answer[ea.BasicProperties.CorrelationId] += "<*>" + response;
                    else
                        Answer.Add(ea.BasicProperties.CorrelationId, response);

                    AnswerCount[ea.BasicProperties.CorrelationId]++;
                }
            };

            channel.BasicConsume(
                consumer: consumer,
                queue: replyQueueName,
                autoAck: true);
        }

        /// <summary>
        /// Lists all the files on a certain path. 
        /// </summary>
        /// <param name="path">string containing the path on the server</param>
        /// <param name="timeout"></param>
        /// <param name="_list">irá conter a resposta do servidor com a respetiva listagem</param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int List(string path, int timeout, out string[] _list)
        {
            string message = "list<*>" + path;
            string response = RequestBroadcast(message, timeout);

            _list = null;

            if (response == null)
                return -1;
            else
                _list = response.Split(new string[] { "<*>" }, StringSplitOptions.RemoveEmptyEntries);

            _list = _list.Distinct().ToArray();

            return 0;
        }

        /// <summary>
        /// Creates an empty folder.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if timeout, -2 if unsuccessful</returns>
        public int CreateDir(string path, int timeout, out bool result)
        {
            string message = "createDir<*>" + path;
            result = false;

            string response = Request(message, timeout);

            if (response == null)
                return -1;
            else if (response.Equals("true"))
                result = true;

            return 0;
        }

        /// <summary>
        /// Creates empty file.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if timeout, -2 if unsuccessful</returns>
        public int CreateFile(string path, int timeout, out bool result)
        {
            return CreateFile(path, null, timeout, out result);
        }

        /// <summary>
        /// Creates File.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="filePath">string containing the path of the file locally</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if timeout, -2 if unsuccessful</returns>
        public int CreateFile(string path, string filePath, int timeout, out bool result)
        {
            string fileMessage;
            string message = "createFile<*>" + path;
            result = false;

            if(filePath != null)
            {
                try
                {
                    byte[] file = File.ReadAllBytes(filePath);
                    fileMessage = Convert.ToBase64String(file);
                }
                catch (IOException)
                {
                    return -1;
                }

                message += "<*>" + fileMessage;
            }

            string response = Request(message, timeout);

            if (response == null)
                return -1;
            else if (response.Equals("true"))
                result = true;

            return 0;
        }

        /// <summary>
        /// Reads a file saved on the server and it's open here (no processment is required by the client).
        /// The file is saved locally on the 'downloads' folder.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int Read(string path, int timeout)
        {
            string message = "read<*>" + path;
            string response = Request(message, timeout);

            if (response == null)
                return -1;

            if (!Directory.Exists("downloads"))
                Directory.CreateDirectory("downloads");

            string[] responseSplit = response.Split(new string[] { "<*>" }, StringSplitOptions.RemoveEmptyEntries);
            string pathFile = "downloads\\" + responseSplit[0];

            try
            {
                byte[] data = Convert.FromBase64String(responseSplit[1]);
                File.WriteAllBytes(pathFile, data);
            }
            catch (Exception ex) when (ex is IOException || ex is IndexOutOfRangeException)
            {
                return -1;
            }

            Process p = new Process();
            ProcessStartInfo ps = new ProcessStartInfo(pathFile);
            ps.UseShellExecute = true;

            p.StartInfo = ps;
            p.Start();

            return 0;
        }

        /// <summary>
        /// Update a file on the server.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="filePath">string containing the path of the file locally</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int Write(string path, string filePath, int timeout, out bool result)
        {
            string message = "write<*>" + path + "<*>";
            result = false;

            try
            {
                byte[] file = File.ReadAllBytes(filePath);
                string fileMessage = Convert.ToBase64String(file);
                message += fileMessage;
            }
            catch (IOException)
            {
                return -1;
            }

            string response = RequestBroadcast(message, timeout);

            if (response == null)
                return -1;

            string[] responseSplit = response.Split(new string[] { "<*>" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < responseSplit.Length; i++)
            {
                if (responseSplit[i].Equals("true"))
                {
                    result = true;
                    break;
                }
            }
            return 0;
        }

        /// <summary>
        /// Check the size of a file.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <param name="result">return the size of the file</param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int Size(string path, int timeout, out long result)
        {
            string message = "size<*>" + path;
            var response = Request(message, timeout);

            result = 0;

            if (response == null)
                return -1;
            else
                result = Int64.Parse(response);

            return 0;
        }

        /// <summary>
        /// Deletes a file or folder.
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int Delete(string path, int timeout, out bool result)
        {
            string message = "delete<*>" + path;
            string response = RequestBroadcast(message, timeout);

            result = false;

            if (response == null)
                return -1;

            string[] responseSplit = response.Split(new string[] { "<*>" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < responseSplit.Length; i++)
            {
                if (responseSplit[i].Equals("true"))
                {
                    result = true;
                    break;
                }
            }
            return 0;
        }

        /// <summary>
        /// Verifies if a certain directory exists or not
        /// </summary>
        /// <param name="path">string containing the path in the server</param>
        /// <param name="timeout"></param>
        /// <param name="result">'true' if everything is okay, 'false' otherwise</param>
        /// <returns>Return 0 if successful, -1 if unsuccessful</returns>
        public int ExistsDir(string path, int timeout, out bool result)
        {
            string message = "exists<*>" + path;
            string response = RequestBroadcast(message, timeout);

            result = false;

            if (response == null)
                return -1;

            string[] responseSplit = response.Split(new string[] { "<*>" }, StringSplitOptions.RemoveEmptyEntries);
            for(int i = 0; i < responseSplit.Length; i++)
            {
                if (responseSplit[i].Equals("true"))
                {
                    result = true;
                    break;
                }
            }
            return 0;
        }

        /// <summary>
        /// Broadcasts request to all servers.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public string RequestBroadcast(string message, int timeout)
        {
            bool cancel = false;

            props = channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            props.CorrelationId = correlationId;
            props.ReplyTo = replyQueueName;
            props.Expiration = ((timeout - 1) * 1000).ToString();

            var body = Encoding.UTF8.GetBytes(message);

            uint numberServers = uint.MaxValue;

            try
            {
                numberServers = channel.ConsumerCount(RPC_QUEUE_NAME);

                channel.BasicPublish(
                    exchange: EXCHANGE_NAME,
                    routingKey: "",
                    basicProperties: props,
                    body: body);
            }
            catch (Exception ex)
            {
                return null;
            }

            AnswerCount.Add(correlationId, 0);

            Timer cancelTimer = new Timer(obj =>
            {
                cancel = true;
            });
            cancelTimer.Change(timeout * 1000, Timeout.Infinite);

            while (AnswerCount.GetValueOrDefault(correlationId, 0) != numberServers && !cancel) ;

            string answer = Answer.GetValueOrDefault(correlationId, null);

            if(AnswerCount.GetValueOrDefault(correlationId, 0) > 0)
                Answer.Remove(correlationId);

            AnswerCount.Remove(correlationId);

            return answer;
        }
        /// <summary>
        /// 1.Wraps the message and send it;
        /// 2.Wait for an answer;
        /// 3.Send back the result (it can be null if something 'bad' happens). 
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <param name="timeout">Time to wait until response unsuccessful.</param>
        /// <returns>Returns the result.</returns>
        private string Request(string message, int timeout)
        {
            bool cancel = false;

            props = channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            props.CorrelationId = correlationId;
            props.ReplyTo = replyQueueName;
            props.Expiration = ((timeout - 1) * 1000).ToString();

            var body = Encoding.UTF8.GetBytes(message);

            try
            {
                channel.BasicPublish(
                    exchange: "",
                    routingKey: RPC_QUEUE_NAME,
                    basicProperties: props,
                    body: body);
            }
            catch (Exception)
            {
                return null;
            }

            AnswerCount.Add(correlationId, 0);

            Timer cancelTimer = new Timer(obj =>
            {
                cancel = true;
            });

            cancelTimer.Change(timeout * 1000, Timeout.Infinite);
            while (AnswerCount.GetValueOrDefault(correlationId, 0) != 1 && !cancel) ;

            if (cancel)
            {
                AnswerCount.Remove(correlationId);
                return null;
            }
            else if (AnswerCount.GetValueOrDefault(correlationId, 0) == 1)
            {
                string answer = Answer.GetValueOrDefault(correlationId, null);
                Answer.Remove(correlationId);
                AnswerCount.Remove(correlationId);

                return answer;
            }

            return null;
        }
    }
}