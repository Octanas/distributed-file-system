﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientLogicLibrary
{
    public static class Extensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
          => dict.TryGetValue(key, out var value) ? value : default(TValue);
        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultValue)
                => dict.TryGetValue(key, out var value) ? value : defaultValue;
    }
}